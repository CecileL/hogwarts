# Bienvenue à Poudlard !

Cloner le repo
Créer une BDD du nom de hogwarts dans mySQL Workbench ou PHPmyAdmin
Lancer le dump hogwarts.sql 
Changer le mot de passe (Password) dans le fichier server.php
Lancer le server avec la commande php -S localhost:8000



### En tant que parent :

Pour consulter les infos de son enfant, le programme auquel il est inscrit ainsi que les cours qui y sont associés et les professeurs qui les animent, mettez simplement l'url suivant : http://localhost:8000/students.php?idstudents=id

(Remplacer simplement le dernier id par l'id de votre enfant **exemple : http://localhost:8000/students.php?idstudents=1**)

*P.S : il n'y a que 5 élèves dans tout Poudlard*



### En tant qu'élève :

Pour consulter votre emploi du temps (et toutes vos informations), mettez simplement l'url suivant : http://localhost:8000/students.php?idstudents=id

(Remplacer simplement le dernier id par votre id **exemple : http://localhost:8000/students.php?idstudents=1**)



### En tant qu'administrateur :

Pour consulter les cours qui ont lieu dans une salle, mettez simplement l'url suivant : http://localhost:8000/courses.php?idclassrooms=id

(Remplacer simplement le dernier id par l'id de la salle **exemple : http://localhost:8000/courses.php?idclassrooms=1**)


Pour connaître les cours présent dans un programme, mettez simplement l'url suivante : http://localhost:8000/programs.php?idprograms=id

(Remplacer simplement le dernier id par l'id du programme voulu **exemple : http://localhost:8000/programs.php?idprograms=1***)

*P.S : Seuls les id 1, 4 et 8 fonctionnent*

-- MySQL dump 10.13  Distrib 5.7.28, for Linux (x86_64)
--
-- Host: localhost    Database: hogwarts
-- ------------------------------------------------------
-- Server version	5.7.28-0ubuntu0.18.04.4

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `appreciations`
--

DROP TABLE IF EXISTS `appreciations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appreciations` (
  `idappreciations` int(11) NOT NULL AUTO_INCREMENT,
  `status` enum('Presence','Retard','Absence') NOT NULL,
  `note` decimal(10,0) NOT NULL,
  `students_idstudents` int(11) NOT NULL,
  `students_programs_idprograms` int(11) NOT NULL,
  `courses_idcourses` int(11) NOT NULL,
  `courses_classrooms_idclassrooms` int(11) NOT NULL,
  `courses_professors_idprofessors` int(11) NOT NULL,
  PRIMARY KEY (`idappreciations`,`students_idstudents`,`students_programs_idprograms`,`courses_idcourses`,`courses_classrooms_idclassrooms`,`courses_professors_idprofessors`),
  KEY `fk_appreciations_students1_idx` (`students_idstudents`,`students_programs_idprograms`),
  KEY `fk_appreciations_courses1_idx` (`courses_idcourses`,`courses_classrooms_idclassrooms`,`courses_professors_idprofessors`),
  CONSTRAINT `fk_appreciations_courses1` FOREIGN KEY (`courses_idcourses`, `courses_classrooms_idclassrooms`, `courses_professors_idprofessors`) REFERENCES `courses` (`idcourses`, `classrooms_idclassrooms`, `professors_idprofessors`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_appreciations_students1` FOREIGN KEY (`students_idstudents`, `students_programs_idprograms`) REFERENCES `students` (`idstudents`, `programs_idprograms`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appreciations`
--

LOCK TABLES `appreciations` WRITE;
/*!40000 ALTER TABLE `appreciations` DISABLE KEYS */;
/*!40000 ALTER TABLE `appreciations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `classrooms`
--

DROP TABLE IF EXISTS `classrooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `classrooms` (
  `idclassrooms` int(11) NOT NULL AUTO_INCREMENT,
  `name` enum('A1','A2','A3','A4','A5','A6','A7','A8','A9','A10','B1','B2','B3','B4','B5','B6') NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  PRIMARY KEY (`idclassrooms`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `classrooms`
--

LOCK TABLES `classrooms` WRITE;
/*!40000 ALTER TABLE `classrooms` DISABLE KEYS */;
INSERT INTO `classrooms` VALUES (1,'A1','2019-12-16','09:00:00'),(2,'A2','2019-12-17','14:00:00'),(3,'A3','2019-12-16','11:00:00'),(4,'A6','2019-12-17','09:00:00'),(5,'B1','2019-12-16','09:00:00'),(6,'B2','2019-12-16','14:00:00'),(7,'B3','2019-12-17','09:00:00'),(8,'B5','2019-12-17','10:30:00'),(9,'A4','2019-12-17','14:00:00'),(10,'A5','2019-12-16','09:00:00'),(11,'A7','2019-12-16','14:00:00'),(12,'A8','2019-12-17','14:00:00');
/*!40000 ALTER TABLE `classrooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `idcourses` int(11) NOT NULL AUTO_INCREMENT,
  `entilted` enum('Arithmancie','Astronomie','Botanique','Defense contre les forces du mal','Divination','Etude des moldus','Etude des runes','Histoire de la magie','Metamorphose','Potions','Soins aux creatures magiques','Sortileges','Vol') NOT NULL,
  `duration` time NOT NULL,
  `classrooms_idclassrooms` int(11) NOT NULL,
  `professors_idprofessors` int(11) NOT NULL,
  PRIMARY KEY (`idcourses`,`classrooms_idclassrooms`,`professors_idprofessors`),
  KEY `fk_courses_classrooms1_idx` (`classrooms_idclassrooms`),
  KEY `fk_courses_professors1_idx` (`professors_idprofessors`),
  CONSTRAINT `fk_courses_classrooms1` FOREIGN KEY (`classrooms_idclassrooms`) REFERENCES `classrooms` (`idclassrooms`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_courses_professors1` FOREIGN KEY (`professors_idprofessors`) REFERENCES `professors` (`idprofessors`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (1,'Vol','02:00:00',2,1),(2,'Metamorphose','04:00:00',1,2),(3,'Potions','02:00:00',6,3),(4,'Soins aux creatures magiques','01:30:00',8,4),(5,'Vol','02:00:00',4,1),(6,'Metamorphose','04:00:00',9,2),(7,'Potions','02:00:00',3,3),(8,'Soins aux creatures magiques','01:30:00',5,4),(9,'Vol','02:00:00',10,1),(10,'Metamorphose','04:00:00',11,2),(11,'Potions','02:00:00',7,3),(12,'Soins aux creatures magiques','01:30:00',12,4);
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses_has_programs`
--

DROP TABLE IF EXISTS `courses_has_programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses_has_programs` (
  `courses_idcourses` int(11) NOT NULL,
  `courses_classrooms_idclassrooms` int(11) NOT NULL,
  `courses_professors_idprofessors` int(11) NOT NULL,
  `programs_idprograms` int(11) NOT NULL,
  PRIMARY KEY (`courses_idcourses`,`courses_classrooms_idclassrooms`,`courses_professors_idprofessors`,`programs_idprograms`),
  KEY `fk_courses_has_programs_programs1_idx` (`programs_idprograms`),
  KEY `fk_courses_has_programs_courses1_idx` (`courses_idcourses`,`courses_classrooms_idclassrooms`,`courses_professors_idprofessors`),
  CONSTRAINT `fk_courses_has_programs_courses1` FOREIGN KEY (`courses_idcourses`, `courses_classrooms_idclassrooms`, `courses_professors_idprofessors`) REFERENCES `courses` (`idcourses`, `classrooms_idclassrooms`, `professors_idprofessors`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_courses_has_programs_programs1` FOREIGN KEY (`programs_idprograms`) REFERENCES `programs` (`idprograms`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses_has_programs`
--

LOCK TABLES `courses_has_programs` WRITE;
/*!40000 ALTER TABLE `courses_has_programs` DISABLE KEYS */;
INSERT INTO `courses_has_programs` VALUES (1,2,1,1),(2,1,2,1),(3,6,3,1),(4,8,4,1),(9,10,1,4),(10,11,2,4),(11,7,3,4),(12,12,4,4),(5,4,1,8),(6,9,2,8),(7,3,3,8),(8,5,4,8);
/*!40000 ALTER TABLE `courses_has_programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `professors`
--

DROP TABLE IF EXISTS `professors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `professors` (
  `idprofessors` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `status` enum('prestataire','salarie') NOT NULL,
  PRIMARY KEY (`idprofessors`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `professors`
--

LOCK TABLES `professors` WRITE;
/*!40000 ALTER TABLE `professors` DISABLE KEYS */;
INSERT INTO `professors` VALUES (1,'BIBINE','Roland','rbibine@hogwarts.com','salarie'),(2,'McGONAGALL','Minerva','mmcgonagall@hogwarts.com','salarie'),(3,'ROGUE','Severus','srogue@hogwarts.com','salarie'),(4,'HAGRID','Rubeus','rhagrid@hogwarts.com','prestataire');
/*!40000 ALTER TABLE `professors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `programs`
--

DROP TABLE IF EXISTS `programs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `programs` (
  `idprograms` int(11) NOT NULL AUTO_INCREMENT,
  `entitled` enum('Magie blanche 1ere Annee','Magie blanche 2eme Annee','Magie blanche 3eme Annee','Magie blanche 4eme Annee','Magie blanche 5eme Annee','Defense contre la magie noire 1ere Annee','Defense contre la magie noire 2eme Annee','Defense contre la magie noire 3eme Annee','Defense contre la magie noire 4eme Annee','Defense contre la magie noire 5eme Annee') NOT NULL,
  PRIMARY KEY (`idprograms`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `programs`
--

LOCK TABLES `programs` WRITE;
/*!40000 ALTER TABLE `programs` DISABLE KEYS */;
INSERT INTO `programs` VALUES (1,'Magie blanche 1ere Annee'),(2,'Magie blanche 2eme Annee'),(3,'Magie blanche 3eme Annee'),(4,'Magie blanche 4eme Annee'),(5,'Magie blanche 5eme Annee'),(6,'Defense contre la magie noire 1ere Annee'),(7,'Defense contre la magie noire 2eme Annee'),(8,'Defense contre la magie noire 3eme Annee'),(9,'Defense contre la magie noire 4eme Annee'),(10,'Defense contre la magie noire 5eme Annee');
/*!40000 ALTER TABLE `programs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `students`
--

DROP TABLE IF EXISTS `students`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `students` (
  `idstudents` int(11) NOT NULL AUTO_INCREMENT,
  `last_name` varchar(50) NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `address` varchar(100) NOT NULL,
  `postal_code` varchar(50) NOT NULL,
  `city` varchar(50) NOT NULL,
  `birth_date` date NOT NULL,
  `registration_date` date NOT NULL,
  `programs_idprograms` int(11) NOT NULL,
  PRIMARY KEY (`idstudents`,`programs_idprograms`),
  KEY `fk_students_programs_idx` (`programs_idprograms`),
  CONSTRAINT `fk_students_programs` FOREIGN KEY (`programs_idprograms`) REFERENCES `programs` (`idprograms`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `students`
--

LOCK TABLES `students` WRITE;
/*!40000 ALTER TABLE `students` DISABLE KEYS */;
INSERT INTO `students` VALUES (1,'POTTER','Harry','400 Oxford Street','W1A 1AB','London','1995-07-31','2019-12-12',1),(2,'WEASLEY','Ronald','80 Buckingham Gate','SW1E 6PD','London','1995-03-01','2019-12-12',1),(3,'GRANGER','Hermione','38 Poland Street','W1F 7LY','London','1995-09-19','2019-12-12',1),(4,'CHANG','Cho','45 Brick Ln','E1 6RH','London','1994-01-16','2019-12-12',8),(5,'DIGGORY','Cedric','65 Hanbury St','E1 5JP','London','1992-11-22','2019-12-12',4);
/*!40000 ALTER TABLE `students` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-13 16:43:52
